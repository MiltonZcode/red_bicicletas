var map = L.map('main_map').setView([-34.8524958246005, -56.136804565398506], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'}).addTo(map);

L.marker([-34.852570110926465, -56.136734563509584]).addTo(map);
L.marker([-34.85097147671028, -56.139574674818235]).addTo(map);
L.marker([-34.853912135296774, -56.14042761724003]).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})


