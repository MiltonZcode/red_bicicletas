var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://127.0.0.1/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('we are connected to test database');
            done();
        });
    });


    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', ()=> {
        it('crea una instancia de bicicleta', ()=> {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);
        });
    });

    describe('Bicicleta.allBicis', ()=> {
        it('comienza vacia', (done)=>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });                
        });
    });

    describe('Bicicleta.add', ()=>{
        it('agregamos solo una bici', (done)=>{            
            var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});            
            Bicicleta.add(aBici, function(err, newBici){
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);

                    done();
                });
            });    
        });
    });    
    
    describe("Bicicleta.findByCode", () => {
        it('Debe devolver la bici con el codigo de 1 ', (done) => {
            
            Bicicleta.allBicis(function(err, bicis){
                
                
                expect(bicis.length).toBe(0)
                
                var bbici = new Bicicleta({code: 1, color: 'verde', modelo: 'urbana', ubicacion: [4.594911, -74.123508]});
                
                Bicicleta.add(bbici, function(err, newBici){
                    if(err)  console.log(err);
                
                    var Bici2 = new Bicicleta({code: 2, color: 'azul', modelo: 'rural', ubicacion: [4.594911, -74.123508]});
                    Bicicleta.add(Bici2, function(err, newBici){
                        if(err)  console.log(err);
                        Bicicleta.findByCode(1, function(err, targetBici){
                            expect(targetBici.code).toBe(bbici.code);
                            expect(targetBici.color).toBe(bbici.color);
                            expect(targetBici.modelo).toBe(bbici.modelo);

                            done();
                        });
                    });
                });
            });
            //var bici = new Bicicleta({code: 24245451, color: 'verde', modelo: 'urbana', ubicacion: [4.594911, -74.123508]});
        });
    });

    describe("Bicicleta.removeByCode", () => {
        it('Debe remover la bici con el codigo de 10 ', (done) => {
            
            Bicicleta.allBicis(function(err, bicis){
                
                
                expect(bicis.length).toBe(0)
                
                var bbici = new Bicicleta({code: 10, color: 'morada', modelo: 'urbana', ubicacion: [4.594911, -74.123508]});
                
                Bicicleta.add(bbici, function(err, newBici){
                    if(err)  console.log(err);
                
                    var Bici2 = new Bicicleta({code: 11, color: 'violeta', modelo: 'rural', ubicacion: [4.594911, -74.123508]});
                    Bicicleta.add(Bici2, function(err, newBici){
                        if(err)  console.log(err);
                        Bicicleta.removeByCode(10, function(err, targetBici){
                            
        
                            expect(targetBici.deletedCount).toBe(1);
                            expect(targetBici.ok).toBe(1);
                            
                            expect(targetBici.code).toBe(undefined);
                            expect(targetBici.color).toBe(undefined);
                            expect(targetBici.modelo).toBe(undefined);

                            done();
                        });
                    });
                });
            });
        });
    });
    

});






// beforeEach(()=>{Bicicleta.allBicis = []; });

// describe('Bicicleta.allBicis', ()=> {
//     it('comienza vacia', ()=>{
//         expect(Bicicleta.allBicis.length).toBe(0);
//     })
// })

// describe('Bicicleta.add', ()=>{
//     it('agregamos una', ()=>{
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var a = new Bicicleta(1, 'rojo', 'urbana', [-34.85448318548923, -56.13802131685186]);
//         Bicicleta.add(a);

//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0]).toBe(a);
//     })
// })
// describe('Bicicleta.findById', ()=>{
//     it('devolver la bici ccon id 1', ()=>{
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var aBici = new Bicicleta(1, 'verde', 'urbana');
//         var aBici2 = new Bicicleta(2, 'rojo', 'montana');

//         Bicicleta.add(aBici);
//         Bicicleta.add(aBici2);

//         var targetBici = Bicicleta.findById(1);

//         expect(targetBici.id).toBe(1);
//         expect(targetBici.color).toBe(aBici.color);
//         expect(targetBici.modelo).toBe(aBici.modelo);
//     })
// })